const express = require('express');
const compress = require('compression');
const bodyParser = require('body-parser')
const { HTTP } = require('./constants.js')
const Database = require('./db');
const Router = require('./router');
const app = express();
const { PORT } = HTTP;

//app.use(compress());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());



app.use(function(req, res, next) {
    next(); // make sure we go to the next routes and don't stop here
});
Router.registerRoutes(app);
module.exports = {
  start: (cb) => { app.listen(PORT, cb.bind(null, PORT)); }
};
