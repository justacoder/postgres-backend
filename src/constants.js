module.exports = {
  HTTP: {
    HOST: 'localhost',
    PORT: 8000,
    PATH: '/'
  },
  RESOURCES: [
    'author',
    'dictionary',
    'frequency',
    'transcription',
    'word',
    'work'
  ],
  DB: {
    WORK_TYPES: [
      'PROSE',
      'POEM'
    ],
    UPLOAD_COLUMNS: [
      'file_location'
    ],
    DATE_TIME_COLUMNS: [
      'created_on',
      'updated_on'
    ],
    FILTER: {
      AND: 'AND',
      OR: 'OR',
      IN: 'IN',
      IS_EQUAL: '=',
      IS_NOT_EQUAL: '!=',
      GREATER: '>',
      LESSER: '<',
      GREATER_OR_EQUAL: '>=',
      LESSER_OR_EQUAL: '<=',
    },
    CONFIG: {
      user: 'postgres',
      database: 'kelimedizin',
      password: '',
      host: 'localhost',
      port: 5432,
      max: 10,
      idleTimeoutMillis: 30000
    }
  }
};
