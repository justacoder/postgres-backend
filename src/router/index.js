const multer = require('multer');
const upload = multer({ dest: 'uploads/' });
const bodyParser = require('body-parser')
const Controller = require('../controller')
const { RESOURCES } = require('../constants.js');
const jsonParser = bodyParser.json()

class RouterFactory {
  static registerRoutes(app) {
    RESOURCES.forEach(this.generateRouteHandlersFor.bind(this, app))
  }

  static generateRouteHandlersFor(router, resource) {
    this.generateListHandlerFor(router, resource);
    this.generateReadHandlerFor(router, resource);
    this.generateCreateHandlerFor(router, resource);
    this.generateReplaceHandlerFor(router, resource);
    this.generateModifyHandlerFor(router, resource);
    this.generateDeleteHandlerFor(router, resource);
    this.generateInfoHandlerFor(router, resource);
    this.generateSchemaHandler(router);
  }

  static generateSchemaHandler(app) {
    app.get(`/schemas/:schema`, Controller.getSchema.bind(Controller))
  }

  static generateInfoHandlerFor(app, resource) {
    app.get(`/${resource}/info`, Controller.info.bind(Controller));
  }
  static generateListHandlerFor(app, resource) {
    app.get(`/${resource}`, Controller.readAll.bind(Controller));
  }

  static generateReadHandlerFor(app, resource) {
    app.get(`/${resource}/:_id`, Controller.read.bind(Controller));
  }

  static generateCreateHandlerFor(app, resource) {
    app.post(`/${resource}`, Controller.create.bind(Controller));
  }

  static generateReplaceHandlerFor(app, resource) {
    app.put(`/${resource}/:_id`, Controller.replace.bind(Controller));
  }

  static generateModifyHandlerFor(app, resource) {
    app.patch(`/${resource}/:_id`, Controller.modify.bind(Controller));
  }

  static generateDeleteHandlerFor(app, resource){
    app.delete(`/${resource}/:_id`, Controller.delete.bind(Controller));
  }

}

module.exports = RouterFactory;
