class QueryError extends Error {
  constructor(...args) {
    super(...args)
    Error.captureStackTrace(this, QueryError)
  }
}

module.exports = {
  QueryError
};
