const fs = require('fs');
const Dictionary = require('./dictionary.js');
const mammoth = require('mammoth');

class FileProcessor {
  static processFile(filePath) {
    return this.extractText(filePath)
      .then(this.extractLines.bind(this))
      .then(this.createDictionary.bind(this))
    .catch((err) => {
      console.log(err);
    });
  }

static extractText(path) {
  return mammoth.extractRawText({ path })
  .then((result) => ({ result }));
}

static extractLines({ result }) {
  return {
  lines: result.value
  .split('\n')
  .filter((line => !!line))
  .filter(line => /((([A-Z])([0-9])+)+(\/)*(([A-Z])*([0-9])+)*)/g.test(line))
  .map((line) => line.split(/((([A-Z])([0-9])+)+(\/)*(([A-Z])*([0-9])+)*)/g))
  .map((split) => ({
      ref: split[1],
      text: split[split.length-1].split('/').map((passage) => passage.trim())
    }))
  };

}

static getWords(line) {
  return line
  .map(this.fixSpaces.bind(this))
  .map(this.fixExtensions.bind(this))
  .map(this.fixCapitalization.bind(this))
  .map((l) => l.split(' '))
  .filter((w) => w.length > 1)
  .reduce((prev, next) =>  prev.concat(next));
}

static fixSpaces(text) {
  return text.split(' ')
  .map((w) => w.trim())
  .filter((w) => !!w.length)
  .join(' ');
}

static fixExtensions(text) {
  return text.replace(' -', '-').replace('.', '');
}

static fixCapitalization(text) {
  return (text.substring(0,1).toLowerCase() + text.substring(1));
}

static getConcordance (words, lines, counts) {
  if( words && lines) {
    return Object
    .keys(words)
    .sort()
    .map((word) => ({ word, numbers: counts[word], lines: words[word].map((ref) => `${ref} ${lines[ref]}`)}));
  }

  return [];
}

static createDictionary({ lines }) {
  let dictionary;
  const refs = {};
  const contents = [];
  const dict = {};
  let words = [];
  let wordCounts = {};
  let letterCounts = {};
  let wordTotal = 0;


  lines.forEach((p) => {
    if(refs[p.ref]) {
      let index = Number(p.ref.substring(1));
      index = index + 1;
      p.ref = `S${index}`;
    }

    refs[p.ref] = true;
    contents.push(`${p.text[0]} / ${p.text[1]}`);
    const newWords = this.getWords(p.text);
    words = [...words, ...newWords];
    newWords.forEach((w) => {
      dict[w] = dict[w] || [];
      wordCounts[w] = wordCounts[w] || { count: 0 };
      wordCounts[w].count += 1;
      wordTotal += 1;
      w
      .split('')
      .forEach((l) => {
        letterCounts[l] = letterCounts[l] ? (letterCounts[l] + 1) : 1;
    });
      if(!dict[w].includes(p.ref)) {
        dict[w].push(p.ref);
      }
    });
  });
  words = words.sort();
  dictionary = Dictionary.generate(words, dict);
  wordCounts = Object
  .keys(wordCounts)
  .map((c) => Object.assign({}, wordCounts[c], { word: c, frequency: wordCounts[c].count / wordTotal }))
  .reduce((prev, next) => Object.assign(prev, { [next.word]: { count: next.count, frequency: next.frequency } }), {});

  this.wordCounts = wordCounts;
  this.dictionary = dictionary;
  const ln = lines.reduce((prev, next) => Object.assign(prev, { [next.ref]: `${next.text[0]} / ${next.text[1]}` }), {});

  const concordance = this.getConcordance(dict, ln, wordCounts);

  console.log('CONCORDANCE\n\n\n', JSON.stringify(concordance, null, 2));
  //console.log('DICTIONARY\n\n\n', dictionary);
  //console.log('CONTENTS\n\n\n', contents);

  return {
    concordance,
    dictionary,
    contents,
  };
}
}

module.exports = FileProcessor;
