const { DB } = require('../constants.js');
const { FILTER } = DB;
const { AND, OR, IN } = FILTER;

module.exports = class Filter {
  static generate(iFormat, values) {
    const { where } = iFormat;

    let query = '';

    if ( where && Object.values(where).length ) {
      query += ' WHERE';

      let [ conditions, currentValues ] = this.handleCondition(where, values);

      return [`${query}${conditions}`, currentValues];
    }

    return [ query, [] ];
  }

  static handleCondition(condition, values) {
    const { op, conditions } = condition;

    if (conditions && conditions.length) {
      const { conditions } = condition;
      let currentValues = values;
      let conditionString = '';

      const query = conditions
      .reduce((prev, next) => {
        [ conditionString, currentValues ] = this.handleCondition(next, currentValues);

        return [ ...prev, conditionString ];
      }, [])
      .join(` ${op}`);

      return [ ` (${query} ) `, currentValues ];
    } else {
      const { lhs, rhs } = condition;
      values.push(rhs);
      const conditionString = ` ${lhs} ${op} $${values.length}`

      return [ conditionString, values ];
    }
  }
}
