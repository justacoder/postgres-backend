const pg = require('pg');
const Tables = require('./seed-tables.js');
const Data = require('./seed-data.js');
const Filter = require('./filter.js');
const { RESOURCES, DB } = require('../constants.js');
const { CONFIG, SCHEMAS } = DB;
const Schema = require('./schemas');

class Database {

  static configure(config = CONFIG) {
    this.config = Object.assign(this.config, config);
    return this;
  }

  static connect() {
    this.pool = new pg.Pool(this.config);
    this.pool.on('error', function (err, client) {
      console.error('idle client error', err.message, err.stack)
    });
    return this;
  }

  static seed(drop=true) {
    return this
      .query(Tables.generate(), [])
      .then(this.initialize.bind(this))
      .then(() => {
        return Promise.all(
          Data
            .map(this.generateInsertQueryFromIF.bind(this))
            .map((q) => this.query(...q))
        );
      });
  }

  static getSchema(schema) {
    return Schema.schemas[schema];
  }

  static convertSchemaToTable(schema) {
    
  }

  static createUser(name, password) {

  }

  static createDatabase(name) {

  }

  static createTable(name) {}

  static dropUser() {}

  static dropDatabase() {}

  static dropTable() {}

  static generateSelectQueryForTable(table, data=null, where=null) {
    return this.generateSelectQueryFromIF(this.generateIF('select', table, data, where));
  }

  static generateInsertQueryForTable(table, data=null, where=null) {
    return this.generateInsertQueryFromIF(this.generateIF('insert', table, data, where));
  }

  static generateUpdateQueryForTable(table, data=null, where=null) {
    return this.generateUpdateQueryFromIF(this.generateIF('update', table, data, where));
  }

  static generateDeleteQueryForTable(table, data=null, where=null) {
    return this.generateDeleteQueryFromIF(this.generateIF('delete', table, data, where));
  }

  static generateIF(type, table, data, where) {
    return {
      type,
      table,
      data,
      where
    };
  }

  static generateSelectQueryFromIF(iFormat) {
    const { data } = iFormat;
    const tableInfo = this.getTableInfo(iFormat.table);

    if (tableInfo) {
      const keys = Object.keys(tableInfo)

      let query = `SELECT ${keys.join(', ')} FROM public.${iFormat.table}`;
      const [ whereClause, values ] = Filter.generate(iFormat, []);

      query += whereClause;
      query = `${query};`;
      return [query, values];
    } else {
      return false;
    }
  }

  static generateInsertQueryFromIF(iFormat) {
    const tableInfo = this.getTableInfo(iFormat.table);
    const keys = Object
      .keys(tableInfo)
      .filter((key) => !!iFormat.data[key]);
    const valuesString = keys
      .map((key) => tableInfo[key])
      .map((type, i) => `$${i+1}::${type}`)
      .join(', ');
    const valuesArray = keys
      .map((key) => iFormat.data[key]);
    let query = `INSERT INTO public.${iFormat.table}(${keys.join(', ')})
    VALUES (${valuesString});`;

    return [query, valuesArray];
  }

  static generateUpdateQueryFromIF(iFormat) {
    const tableInfo = this.getTableInfo(iFormat.table);

    if (tableInfo) {
      const keys = Object
        .keys(tableInfo)
        .filter((key) => !!iFormat.data[key]);

      const updateQuery = Object
        .keys(tableInfo)
        .filter((key) => !!iFormat.data[key])
        .map((key, i) => `${key} = $${i+1}::${tableInfo[key]}`)
        .join(', ');
      const valuesArray = keys
        .map((key) => iFormat.data[key]);
      let query = `UPDATE public.${iFormat.table} SET ${updateQuery}`;
      const [ whereClause, values ] = Filter.generate(iFormat, valuesArray);

      query += whereClause;
      query = `${query};`;
      return [query, values];
    } else {
      return false;
    }
  }

  static generateDeleteQueryFromIF(iFormat) {
    const tableInfo = this.getTableInfo(iFormat.table);

    if (tableInfo) {
      let query = `DELETE FROM public.${iFormat.table} `;
      const [ whereClause, values ] = Filter.generate(iFormat, []);

      query += whereClause;
      query = `${query};`;
      return [query, values];
    } else {
      return false;
    }
  }

  static initialize() {
    return Promise
      .all(RESOURCES.map((table) => this
        .fetchTableInfo(table)))
      .then((payload) => {
        this.tables = payload
          .map(({ name , rows }) => ({ [name]: rows.map(({ column_name, data_type }) => ({ [column_name]: data_type })).reduce((prev, next) => Object.assign(prev, next), {}) }))
          .reduce((prev, next) => Object.assign(prev, next), {});
        //console.log(JSON.stringify(this.tables, null, 2));
        return this;
      }).catch();
  }

  static fetchTableInfo(tableName) {
    return new Promise((resolve, reject) => {
      const queryString = `select column_name, data_type from INFORMATION_SCHEMA.COLUMNS where table_name = '${tableName}';`;
      this.pool.query(queryString, [], (err, payload) => {
        if(err) {
          reject(err);
        } else {
          resolve({ name: tableName , rows: payload.rows });
        }
      });
    });
  }

  static getTableInfo(tableName) {
    return this.tables[tableName];
  }

  static query(queryString, values=[]) {
/*    console.log(`RUNNING QUERY =>*/
      //${queryString}
      //With ----------------------------------
      /*${JSON.stringify(values, null, 2)}`);*/
    return new Promise((resolve, reject) => {
      this.pool.query(queryString, values, (err, payload) => {
        if(err) {
          reject(err);
        } else {
          resolve(payload);
        }
      });
    });
  }

}

Database.tables = {};
Database.config = {};
Database.pool = null;

module.exports = Database;
