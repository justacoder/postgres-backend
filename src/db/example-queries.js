module.exports = {
  SELECT: [
    {
      type: 'select',
      table: 'author',
      where: {
        lhs: 'public.author.name',
        rhs: 'Neyzen Tevfik'
      }
    }
  ]
};
