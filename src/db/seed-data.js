module.exports = [
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'a',
      lookup: 'a',
      capitalized: 'A',
      alphabetical_order: 1
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'À',
      lookup: 'a',
      capitalized: 'Á',
      alphabetical_order: 2
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'èa',
      lookup: 'a',
      capitalized: 'èA',
      alphabetical_order: 3
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'èÀ',
      lookup: 'a',
      capitalized: 'èÁ',
      alphabetical_order: 4
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'b',
      lookup: 'b',
      capitalized: 'B',
      alphabetical_order: 5
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'c',
      lookup: 'c',
      capitalized: 'C',
      alphabetical_order: 6
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'ç',
      lookup: 'c',
      capitalized: 'Ç',
      alphabetical_order: 7
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'd',
      lookup: 'd',
      capitalized: 'D',
      alphabetical_order: 8
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'ê',
      lookup: 'd',
      capitalized: 'ë',
      alphabetical_order: 9
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'èe',
      lookup: 'e',
      capitalized: 'èE',
      alphabetical_order: 10
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'f',
      lookup: 'f',
      capitalized: 'F',
      alphabetical_order: 11
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'g',
      lookup: 'g',
      capitalized: 'G',
      alphabetical_order: 12
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'ğ',
      lookup: 'g',
      capitalized: 'G',
      alphabetical_order: 13
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'à',
      lookup: 'g',
      capitalized: 'á',
      alphabetical_order: 14
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'h',
      lookup: 'h',
      capitalized: 'H',
      alphabetical_order: 15
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'ó',
      lookup: 'h',
      capitalized: 'Ó',
      alphabetical_order: 16
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'ò',
      lookup: 'h',
      capitalized: 'Ò',
      alphabetical_order: 17
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'ı',
      lookup: 'ı',
      capitalized: 'I',
      alphabetical_order: 18
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'èı',
      lookup: 'ı',
      capitalized: 'èI',
      alphabetical_order: 19
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'i',
      lookup: 'i',
      capitalized: 'İ',
      alphabetical_order: 20
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'ì',
      lookup: 'i',
      capitalized: 'Ì',
      alphabetical_order: 21
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'èi',
      lookup: 'i',
      capitalized: 'èİ',
      alphabetical_order: 22
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'èì',
      lookup: 'i',
      capitalized: 'èÌ',
      alphabetical_order: 23
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'j',
      lookup: 'j',
      capitalized: 'J',
      alphabetical_order: 24
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'k',
      lookup: 'k',
      capitalized: 'K',
      alphabetical_order: 25
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'ú',
      lookup: 'k',
      capitalized: 'Ú',
      alphabetical_order: 26
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'l',
      lookup: 'l',
      capitalized: 'L',
      alphabetical_order: 27
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'm',
      lookup: 'm',
      capitalized: 'M',
      alphabetical_order: 28
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'n',
      lookup: 'n',
      capitalized: 'N',
      alphabetical_order: 29
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'o',
      lookup: 'o',
      capitalized: 'O',
      alphabetical_order: 30
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'èo',
      lookup: 'o',
      capitalized: 'èO',
      alphabetical_order: 31
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'ö',
      lookup: 'ö',
      capitalized: 'Ö',
      alphabetical_order: 32
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'èö',
      lookup: 'ö',
      capitalized: 'èÖ',
      alphabetical_order: 33
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'p',
      lookup: 'p',
      capitalized: 'P',
      alphabetical_order: 34
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'r',
      lookup: 'r',
      capitalized: 'R',
      alphabetical_order: 35
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 's',
      lookup: 's',
      capitalized: 's',
      alphabetical_order: 36
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'ã',
      lookup: 's',
      capitalized: 'ä',
      alphabetical_order: 37
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'å',
      lookup: 's',
      capitalized: 'å',
      alphabetical_order: 38
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'ş',
      lookup: 'ş',
      capitalized: 'Ş',
      alphabetical_order: 39
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 't',
      lookup: 't',
      capitalized: 'T',
      alphabetical_order: 40
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'ù',
      lookup: 't',
      capitalized: 'Ù',
      alphabetical_order: 41
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'u',
      lookup: 'u',
      capitalized: 'U',
      alphabetical_order: 42
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'ÿ',
      lookup: 'u',
      capitalized: 'ß',
      alphabetical_order: 43
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'èu',
      lookup: 'u',
      capitalized: 'èU',
      alphabetical_order: 44
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'èÿ',
      lookup: 'u',
      capitalized: 'èß',
      alphabetical_order: 45
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'ü',
      lookup: 'ü',
      capitalized: 'Ü',
      alphabetical_order: 46
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'èü',
      lookup: 'ü',
      capitalized: 'èÜ',
      alphabetical_order: 47
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'v',
      lookup: 'v',
      capitalized: 'V',
      alphabetical_order: 48
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'y',
      lookup: 'y',
      capitalized: 'Y',
      alphabetical_order: 49
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'z',
      lookup: 'z',
      capitalized: 'Z',
      alphabetical_order: 50
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'ô',
      lookup: 'z',
      capitalized: 'Ô',
      alphabetical_order: 51
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'õ',
      lookup: 'z',
      capitalized: 'Õ',
      alphabetical_order: 52
    }
  },
  {
    type: 'insert',
    table: 'transcription',
    data: {
      actual: 'ø',
      lookup: 'z',
      capitalized: 'Ø',
      alphabetical_order: 53
    }
  },
  {
    type: 'insert',
    table: 'author',
    data: {
      name: 'Neyzen Tevfik'
    }
  },
  {
    type: 'insert',
    table: 'author',
    data: {
      name: 'Mehmet Âkif Ersoy'
    }
  },
  {
    type: 'insert',
    table: 'work',
    data: {
      name: 'Istiklal Marsi',
      author_id: 1,
      type: 1,
      file_location: 'foo'
    }
  },
  {
    type: 'insert',
    table: 'work',
    data: {
      name: 'Hic',
      author_id: 2,
      type: 1,
      file_location: 'bar'
    }
  }
];
