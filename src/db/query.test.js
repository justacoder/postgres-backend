const { DB } = require('../constants.js');
const { QueryError } = require('../exceptions');

describe('Query Class', () => {
  let Query;
  it('should exist', () => {
    expect(Query = require('./query.js')).toBeDefined();
  });

  describe('start method', () => {
    it('should exist', () => {
      expect(Query.start).toBeInstanceOf(Function);
    });

    it('should be chainable', () => {
      expect(Query.start()).toEqual(Query);
    });

    it('should update the query status', () => {
      Query.start()
      expect(Query.status).toEqual(true);
    });
  });

  describe('end method', () => {
    it('should exist', () => {
      expect(Query.end).toBeInstanceOf(Function);
    });

    it('should be chainable', () => {
      expect(Query.end()).toEqual(Query);
    });

    it('should update the query status', () => {
      Query.start();
      Query.end()
      expect(Query.status).toEqual(false);
    });
  });

  describe('reset method', () => {
    it('should exist', () => {
      expect(Query.reset).toBeInstanceOf(Function);
    });

    it('should be chainable', () => {
      expect(Query.reset()).toEqual(Query);
    });

    it('should update the query status', () => {
      Query.start().append('FOOO').reset();
      expect(Query.status).toEqual(false);
      expect(Query.current).toEqual('');
    });
  });

  describe('append method', () => {
    it('should exist', () => {
      expect(Query.append).toBeInstanceOf(Function);
    });

    it('should throw an exception if the query status is false', () => {
      expect(Query.reset().append.bind(Query)).toThrow(QueryError);
    });

    it('should be chainable', () => {
      expect(Query.start().append()).toEqual(Query);
    });

    it('should append the given string to the current one with a space in beetween', () => {
      expect(Query.start().append('foo').append('bar').current).toEqual('foo bar');
    });
  });

  describe('select method', () => {
    it('should exist', () => {
      expect(Query.select).toBeInstanceOf(Function);
    });

    it('should throw an exception if the query status is false', () => {
      Query.end();
      expect(Query.select.bind(Query)).toThrow(QueryError);
    });

    it('should be chainable', () => {
      expect(Query.start().select()).toEqual(Query);
    });

    it('should append the select statement for given columns', () => {
      expect(Query.start().select('foo', 'bar')).toEqual(Query);
    });
  });

  describe('from method', () => {
    it('should exist', () => {
      expect(Query.from).toBeInstanceOf(Function);
    });

    it('should be chainable', () => {
      expect(Query.from()).toEqual(Query);
    });
  });

  describe('where method', () => {
    it('should exist', () => {
      expect(Query.where).toBeInstanceOf(Function);
    });

    it('should be chainable', () => {
      expect(Query.where()).toEqual(Query);
    });
  });

  describe('create method', () => {
    it('should add the generic SQL create statement to the query', () => {
      const spy = jest.spyOn(Query, 'append');

      Query.create('TABLE');
      expect(spy).toHaveBeenCalledWith('CREATE');
      expect(spy).toHaveBeenCalledWith('TABLE');
      spy.mockReset();
      spy.mockRestore();
    });
  });

  describe('withName method', () => {
    it('should exist', () => {
      expect(Query.create).toBeInstanceOf(Function);
    });

    it('should be chainable', () => {
      expect(Query.create('TABLE')).toEqual(Query);
    });

    it('should add the given string to the current query', () => {
      const spy = jest.spyOn(Query, 'append');

      Query.withName('hahaha');
      expect(spy).toHaveBeenCalledWith('hahaha');
      spy.mockReset();
      spy.mockRestore();
    });
  });
});
