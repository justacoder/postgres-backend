const Ajv = require('ajv');
const glob = require('glob');
const ajv = new Ajv();

const { HTTP } = require('../../constants.js');
const { HOST, PORT, PATH } = HTTP;

const schemas = glob
.sync('src/db/schemas/**/*.json')
  .reduce((obj, path) => {
    const schema = require(`.${path.substring(path.lastIndexOf('/'))}`);

    obj[schema.$id] = schema;
    ajv.addSchema(schema);
    return obj;
  }, {});


const validators = Object
.keys(schemas)
.reduce((prev, next) => {
  prev[next] = ajv.compile(schemas[next]);
  return prev;
}, {});

class Parser {
  constructor(validator) {
    this.current = validator;
  }

  getTableSQL() {
    const { uniteSchemaProperties } = this.constructor;
    const { schema } = this.current;
    const { allOf } = schema;
    let refSchema = null;
    let unitedSchema = schema;

    if (Array.isArray(allOf)) {
      allOf.forEach((extra) => {
        const { $ref, type } = extra;

        if ($ref) {
          const refValidator = this.getRefValidator($ref);

          refSchema = refValidator.schema || refValidator;
        } else if (type && type === 'object') {
          refSchema = extra;
        }

        unitedSchema.properties = uniteSchemaProperties(unitedSchema, refSchema);
      });
    }

    Object
      .keys(
        unitedSchema.properties
      )
      .forEach((property) => {
        if (unitedSchema.properties[property].$ref) {
        }
      });
    return unitedSchema;
  }

  static getColumnSQL(columnName, columnInfo) {
    let colName = columnName;
    const { getColumnType } = this;
    const constraints = ['NOT NULL'];

    if (columnName.indexOf('_id') >= 0) {
      if (columnName !== '_id') {
        colName = `${columnName}_id`;
        constraints.push(`REFERENCES ${columnName.split('_id')[0]}(_id)`);
      } else {
        constraints.push('PRIMARY KEY');
      }
    }

    const colType = getColumnType(columnName, columnInfo);

    return `${columnName} ${colType} ${constraints.join(' ')}`;
  }

  static getColumnType(columnName, columnInfo) {
    const { type } = columnInfo;

    if (columnName === '_id') {
      return 'SERIAL';
    }

    switch(columnInfo.type) {
      case 'string':
        const { format } = columnInfo;

        if (columnInfo.enum) {
          return `${columnName}_TYPE`
        }

        if (format === 'date-time') {
          return 'TIMESTAMPTZ';
        }

        return 'TEXT';

        break;
      case 'integer':
        return 'INTEGER';
        break;
      case 'object':
        return `${columnName}_TYPE`;
        break;
    }
  }

  static uniteSchemaProperties( ...schemas ) {
    return schemas.reduce((prev, next) => {
      return Object.assign({}, prev, next.properties);
    }, {});
  }

  getRefValidator(ref) {
    const { refs, refVal } = this.current;

    return refVal[refs[ref]];
  }
}

module.exports = {
  schemas,
  validators,
  Parser
};
