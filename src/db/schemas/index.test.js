const glob = require('glob');
const schemas = glob
.sync('src/db/schemas/**/*.json')
  .reduce((obj, path) => {
    const schema = require(`.${path.substring(path.lastIndexOf('/'))}`);

    obj[schema.$id] = schema;
    return obj;
  }, {});

describe('Schema Module', () => {
  let Schema;
  it('should exist', () => {
    expect(Schema = require('./index.js')).toBeDefined();
  });

  it('should have schemas and validators defined', () => {
    expect(Schema.schemas).toBeDefined();
    expect(Schema.validators).toBeDefined();
  });


  it('should be able to validate json objects against loaded schemas', () => {
    const authorValidator = Schema.validators['author.json'];

    expect(authorValidator({
      "_id": 1,
      "name": "Neyzen Tevfik",
      "created_on": (new Date(1500578359000)).toISOString(),
      "updated_on": (new Date(1500578359000)).toISOString()
    })).toBe(true);
  });

  it('should be able to validate json objects with references against loaded schemas', () => {
    const workValidator = Schema.validators['work.json'];
    const result = workValidator({
      "_id": 1,
      "name": "FooBar",
      "author": {
        "_id": 1,
        "name": "Neyzen Tevfik",
        "created_on": (new Date(1500578359000)).toISOString(),
        "updated_on": (new Date(1500578359000)).toISOString()
      },
      "created_on": (new Date(1500578359000)).toISOString(),
      "updated_on": (new Date(1500578359000)).toISOString()
    });

    expect(result).toBe(true);
  });

  describe('Parser Class', () => {
    it('should be defined', () => {
      expect(Schema.Parser).toBeDefined();
    });

    describe('getTableSQL method', () => {
      it('should be defined', () => {
        expect(Schema.Parser.prototype.getTableSQL).toBeInstanceOf(Function);
      });

      it('should be able to parse a schema into SQL Table Creation Script', () => {
        const workValidator = Schema.validators['work.json'];
        const parser = new Schema.Parser(workValidator);
        const tableInfo = parser.getTableSQL();


        console.log('TABLE INFO YOOOO', tableInfo);
        expect(tableInfo).toBeDefined();
      });
    });

    describe('getColumnSQL method', () => {
      it('should be defined', () => {
        expect(Schema.Parser.getColumnSQL).toBeInstanceOf(Function);
      });

      it('should return the correct SQL for the column', () => {
        expect(Schema.Parser.getColumnSQL('FUKU', {
          type: 'string'
        })).toEqual('FUKU TEXT NOT NULL');

        expect(Schema.Parser.getColumnSQL('FUKUFUKU', {
          type: 'object'
        })).toEqual('FUKUFUKU FUKUFUKU_TYPE NOT NULL');

        expect(Schema.Parser.getColumnSQL('author_id', {
          type: 'integer'
        })).toEqual(`author_id INTEGER NOT NULL REFERENCES author(_id)`);

        expect(Schema.Parser.getColumnSQL('FUKUFUKU', {
          type: 'integer'
        })).toEqual('FUKUFUKU INTEGER NOT NULL');


        expect(Schema.Parser.getColumnSQL('FUKUFUKU', {
          type: 'string'
        })).toEqual('FUKUFUKU TEXT NOT NULL');

        expect(Schema.Parser.getColumnSQL('_id', {
          type: 'integer'
        })).toEqual('_id SERIAL NOT NULL PRIMARY KEY');

        expect(Schema.Parser.getColumnSQL('FUKUFUKU', {
          type: 'string',
          enum: ['foo', 'bar', 'baz']
        })).toEqual('FUKUFUKU FUKUFUKU_TYPE NOT NULL');

        expect(Schema.Parser.getColumnSQL('FUKUFUKU', {
          type: 'string',
          format: 'date-time'
        })).toEqual('FUKUFUKU TIMESTAMPTZ NOT NULL');
      });
    });
    describe('getColumnType method', () => {
      it('should be defined', () => {
        const workValidator = Schema.validators['work.json'];
        const parser = new Schema.Parser(workValidator);

        expect(Schema.Parser.getColumnType).toBeInstanceOf(Function);
      });

      it('should return the proper SQL type for given arguments', () => {
        const workValidator = Schema.validators['work.json'];
        const parser = new Schema.Parser(workValidator);

        expect(Schema.Parser.getColumnType('FUKUFUKU', {
          type: 'object'
        })).toEqual('FUKUFUKU_TYPE');


        expect(Schema.Parser.getColumnType('FUKUFUKU', {
          type: 'integer'
        })).toEqual('INTEGER');


        expect(Schema.Parser.getColumnType('FUKUFUKU', {
          type: 'string'
        })).toEqual('TEXT');

        expect(Schema.Parser.getColumnType('_id', {
          type: 'integer'
        })).toEqual('SERIAL');

        expect(Schema.Parser.getColumnType('author_id', {
          type: 'integer'
        })).toEqual(`INTEGER`);

        expect(Schema.Parser.getColumnType('FUKUFUKU', {
          type: 'string',
          enum: ['foo', 'bar', 'baz']
        })).toEqual('FUKUFUKU_TYPE');

        expect(Schema.Parser.getColumnType('FUKUFUKU', {
          type: 'string',
          format: 'date-time'
        })).toEqual('TIMESTAMPTZ');

      });

    });

    describe('getRefValidator method', () => {
      it('should be defined', () => {
        expect(Schema.Parser.prototype.getRefValidator).toBeInstanceOf(Function);
      });

      it('should be able to return the referenced schema', () => {
        const workValidator = Schema.validators['work.json'];
        const parser = new Schema.Parser(workValidator);

        expect(parser.getRefValidator('author.json')).toBe(workValidator.refVal[1]);
      });
    });

  });
});
