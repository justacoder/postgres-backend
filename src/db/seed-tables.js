const { DB } = require('../constants.js');
const { TABLES, CONFIG } = DB;

class Tables {

  static generateTable(table) {

  }

  static createTable(table) {
    const { name, columns } = table;

    return `
    CREATE TABLE ${name} (
      ${columns.map(this.generateColumn.bind(this)).join(',\n')}
    )
    `
  }

  static dropTable(table) {
    const { name } = table;

    return `
    DROP TABLE IF EXISTS ${name};
    DROP SEQUENCE IF EXISTS ${name}_id_seq;
    `
  }

  static generateColumn(column) {
    const { name, type, init } = column;

    return `${name} ${type} NOT NULL ${init ? init : ''}`;
  }

  static generate(drop=true) {
    let query = drop ? `DROP TABLE IF EXISTS public.transcription;
    DROP TABLE IF EXISTS public.author;
    DROP TABLE IF EXISTS public.work;
    DROP TABLE IF EXISTS public.word;
    DROP TABLE IF EXISTS public.frequency;
    DROP TABLE IF EXISTS public.dictionary;
    DROP SEQUENCE IF EXISTS transcription_id_seq;
    DROP SEQUENCE IF EXISTS author_id_seq;
    DROP SEQUENCE IF EXISTS work_id_seq;
    DROP SEQUENCE IF EXISTS word_id_seq;
    DROP SEQUENCE IF EXISTS frequency_id_seq;
    DROP SEQUENCE IF EXISTS dictionary_id_seq;
    ` : ``;
    query +=
      `
    CREATE SEQUENCE transcription_id_seq;
    CREATE TABLE public.transcription
    (
      _id bigint NOT NULL DEFAULT nextval('transcription_id_seq'::regclass),
      actual character varying(2) COLLATE pg_catalog."default" NOT NULL,
      lookup character varying(2) COLLATE pg_catalog."default" NOT NULL,
      alphabetical_order smallint NOT NULL,
      capitalized character varying(2) COLLATE pg_catalog."default" NOT NULL,
      created_on bigint NOT NULL default extract(epoch from now()),
      updated_on bigint NOT NULL default extract(epoch from now())
    )
    WITH (
      OIDS = FALSE
    )
    TABLESPACE pg_default;
    ALTER TABLE public.transcription
    OWNER to ${CONFIG.user};

    CREATE SEQUENCE author_id_seq;
    CREATE TABLE public.author
    (
      _id bigint NOT NULL DEFAULT nextval('author_id_seq'::regclass),
      name character varying(255) COLLATE pg_catalog."default" NOT NULL,
      created_on bigint NOT NULL default extract(epoch from now()),
      updated_on bigint NOT NULL default extract(epoch from now()),

      CONSTRAINT author_pkey PRIMARY KEY (_id),
      CONSTRAINT "authorName_unique" UNIQUE (name)
    )
    WITH (
      OIDS = FALSE
    )
    TABLESPACE pg_default;

    ALTER TABLE public.author
    OWNER to ${CONFIG.user};

    CREATE UNIQUE INDEX "nameIndex"
    ON public.author USING btree
    (name COLLATE pg_catalog."default" varchar_pattern_ops DESC)
    TABLESPACE pg_default;

    ALTER TABLE public.author
    CLUSTER ON "nameIndex";

    CREATE SEQUENCE work_id_seq;
    CREATE TABLE public.work
    (
      _id bigint NOT NULL DEFAULT nextval('work_id_seq'::regclass),
      author_id bigint NOT NULL,
      name character varying(255) COLLATE pg_catalog."default" NOT NULL,
      type smallint NOT NULL,
      file_location character varying(1023) COLLATE pg_catalog."default" NOT NULL,
      created_on bigint NOT NULL default extract(epoch from now()),
      updated_on bigint NOT NULL default extract(epoch from now()),

      CONSTRAINT work_pkey PRIMARY KEY (_id),
      CONSTRAINT "uniqueAuthorIdName" UNIQUE (author_id, name)
    )
    WITH (
      OIDS = FALSE
    )
    TABLESPACE pg_default;

    ALTER TABLE public.work
    OWNER to ${CONFIG.user};

    CREATE INDEX "author_id_fk"
    ON public.work USING btree
    (author_id)
    TABLESPACE pg_default;

    ALTER TABLE public.work
    CLUSTER ON "author_id_fk";

    CREATE SEQUENCE word_id_seq;
    CREATE TABLE public.word
    (
      _id bigint NOT NULL DEFAULT nextval('word_id_seq'::regclass),
      word character varying(127) COLLATE pg_catalog."default" NOT NULL,
      root character varying(127) COLLATE pg_catalog."default" NOT NULL,
      created_on bigint NOT NULL default extract(epoch from now()),
      updated_on bigint NOT NULL default extract(epoch from now()),

      CONSTRAINT word_pkey PRIMARY KEY (_id)
    )
    WITH (
      OIDS = FALSE
    )
    TABLESPACE pg_default;

    ALTER TABLE public.word
    OWNER to ${CONFIG.user};

    CREATE UNIQUE INDEX "wIndex"
    ON public.word USING btree
    (word COLLATE pg_catalog."default")
    TABLESPACE pg_default;

    ALTER TABLE public.word
    CLUSTER ON "wIndex";
    CREATE SEQUENCE frequency_id_seq;
    CREATE TABLE public.frequency
    (
      _id bigint NOT NULL DEFAULT nextval('frequency_id_seq'::regclass),
      dict_id bigint NOT NULL,
      work_id bigint NOT NULL,
      refNumber integer NOT NULL,
      refType smallint NOT NULL,
      created_on bigint NOT NULL default extract(epoch from now()),
      updated_on bigint NOT NULL default extract(epoch from now()),

      CONSTRAINT frequency_pkey PRIMARY KEY (_id)
    )
    WITH (
      OIDS = FALSE
    )
    TABLESPACE pg_default;

    ALTER TABLE public.frequency
    OWNER to ${CONFIG.user};

    CREATE INDEX "wordLookup"
    ON public.frequency USING btree
    (dict_id)
    TABLESPACE pg_default;

    CREATE INDEX "referenceLookup"
    ON public.frequency USING btree
    (refNumber)
    TABLESPACE pg_default;

    CREATE INDEX "workLookup"
    ON public.frequency USING btree
    (work_id)
    TABLESPACE pg_default;

    CREATE SEQUENCE dictionary_id_seq;

    CREATE TABLE public.dictionary
    (
      _id bigint NOT NULL DEFAULT nextval('dictionary_id_seq'::regclass),
      meaning character varying(2047) COLLATE pg_catalog."default" NOT NULL,
      word_id bigint NOT NULL,
      created_on bigint NOT NULL default extract(epoch from now()),
      updated_on bigint NOT NULL default extract(epoch from now()),

      CONSTRAINT dictionary_pkey PRIMARY KEY (_id)
    )
    WITH (
      OIDS = FALSE
    )
    TABLESPACE pg_default;

    ALTER TABLE public.dictionary
    OWNER to postgres;


    CREATE UNIQUE INDEX "wordIndex"
    ON public.dictionary USING btree
    (word_id)
    TABLESPACE pg_default;

    ALTER TABLE public.dictionary
    CLUSTER ON "wordIndex";
    `;

    return query;
  }
}

module.exports = Tables;
