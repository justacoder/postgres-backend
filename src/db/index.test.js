const { DB } = require('../constants.js');

describe('Database Class', () => {
  let Database;
  it('should exist', () => {
    expect(Database = require('./index.js')).toBeDefined();
    Database.tables = {
      "author": {
        "_id": "bigint",
        "name": "character varying",
        "created_on": "bigint",
        "updated_on": "bigint"
      },
      "dictionary": {
        "_id": "bigint",
        "meaning": "character varying",
        "word_id": "bigint",
        "created_on": "bigint",
        "updated_on": "bigint"
      },
      "frequency": {
        "_id": "bigint",
        "dict_id": "bigint",
        "work_id": "bigint",
        "refnumber": "integer",
        "reftype": "smallint",
        "created_on": "bigint",
        "updated_on": "bigint"
      },
      "transcription": {
        "_id": "bigint",
        "actual": "character varying",
        "lookup": "character varying",
        "alphabetical_order": "smallint",
        "capitalized": "character varying",
        "created_on": "bigint",
        "updated_on": "bigint"
      },
      "word": {
        "_id": "bigint",
        "word": "character varying",
        "root": "character varying",
        "created_on": "bigint",
        "updated_on": "bigint"
      },
      "work": {
        "_id": "bigint",
        "author_id": "bigint",
        "name": "character varying",
        "type": "smallint",
        "file_location": "character varying",
        "created_on": "bigint",
        "updated_on": "bigint"
      }
    }
  });

  describe('configure method', () => {
    it('should apply the default options if none is passed as an argument', () => {
      Database.configure();
      expect(Database.config).toEqual(DB.CONFIG);
    });

    it('should apply the options passed as an argument', () => {
      Database.configure({ foo: 'bar' });
      expect(Database.config.foo).toEqual('bar');
    });
  });

  describe('connect method', () => {
    it('should create a pool of connections', () => {
      Database.connect();
      expect(Database.pool).toBeDefined();
    });
  });

  describe('generateSelectQueryForTable method', () => {
    it('should return false if the given table doesn\'t exist', () => {
      expect(Database.generateSelectQueryForTable('foo')).toEqual(false);
    });

    it('should generate IF', () => {
      const spy = jest.spyOn(Database, 'generateIF');

      expect(Database.generateSelectQueryForTable('author')).toEqual([
        'SELECT _id, name, created_on, updated_on FROM public.author;',
        []
      ]);
      expect(spy).toHaveBeenCalledWith('select', 'author', null, null);

      spy.mockReset();
      spy.mockRestore();
    });

    it('should generate IF with a where clause that has only one condition', () => {
      expect(Database.generateSelectQueryForTable('author', null, {
        lhs: 'name',
        rhs: 'Neyzen Tevfik',
        op: '='
      })).toEqual([
        'SELECT _id, name, created_on, updated_on FROM public.author WHERE name = $1;',
        ['Neyzen Tevfik']
      ]);

    });

    it('should be able to handle more complicated where clauses', () => {
      const now = Date.now();
      expect(Database.generateSelectQueryForTable('author', null, {
        conditions: [
          {
            lhs: 'name',
            rhs: 'Neyzen Tevfik',
            op: '='
          },
          {
            lhs: 'created_on',
            rhs: now,
            op: '<'
          }
        ],
        op: 'AND'
      })).toEqual([
        'SELECT _id, name, created_on, updated_on FROM public.author WHERE ( name = $1 AND created_on < $2 ) ;',
        ['Neyzen Tevfik', now]
      ]);
    });

    it('should be able to handle even more complicated where clauses', () => {
      const now = Date.now();
      expect(Database.generateSelectQueryForTable('author', null, {
        conditions: [
          {
            conditions: [
              {
                lhs: '_id',
                rhs: 10,
                op: '>'
              },
              {
                lhs: 'updated_on',
                rhs: now,
                op: '='
              }
            ],
            op: 'OR'
          },
          {
            lhs: 'name',
            rhs: 'Neyzen Tevfik',
            op: '='
          },
          {
            lhs: 'created_on',
            rhs: now,
            op: '<'
          }
        ],
        op: 'AND'
      })).toEqual([
        'SELECT _id, name, created_on, updated_on FROM public.author WHERE ( ( _id > $1 OR updated_on = $2 )  AND name = $3 AND created_on < $4 ) ;',
        [10, now, 'Neyzen Tevfik', now]
      ]);
    });
  });

  describe('generateUpdateQueryForTable method', () => {
    it('should be able to correctly generate an update query', ()=>{
      const now = Date.now();

      expect(Database.generateUpdateQueryForTable('word', {
        word: "barbar",
        root: "yoyoyoyo",
      }, {
        conditions: [
          {
            conditions: [
              {
                lhs: '_id',
                rhs: 10,
                op: '='
              },
              {
                lhs: 'updated_on',
                rhs: now,
                op: '='
              }
            ],
            op: 'OR'
          },
          {
            lhs: 'root',
            rhs: 'foo',
            op: '='
          }
        ],
        op: 'AND'
      })).toEqual([
        'UPDATE public.word SET word = $1::character varying, root = $2::character varying WHERE ( ( _id = $3 OR updated_on = $4 )  AND root = $5 ) ;',
        ['barbar', 'yoyoyoyo', 10, now, 'foo']
      ]);
    });
  });

  describe('generateDeleteQueryForTable method', () => {
    it('should be able to correctly generate a delete query', () => {
      const now = Date.now();

      expect(Database.generateDeleteQueryForTable('word', null, {
        conditions: [
          {
            conditions: [
              {
                lhs: '_id',
                rhs: 1,
                op: '='
              },
              {
                lhs: 'updated_on',
                rhs: now,
                op: '='
              }
            ],
            op: 'OR'
          },
          {
            lhs: 'root',
            rhs: 'foo',
            op: '='
          }
        ],
        op: 'AND'
      })).toEqual([
        'DELETE FROM public.word  WHERE ( ( _id = $1 OR updated_on = $2 )  AND root = $3 ) ;',
        [1, now, 'foo']
      ]);
    });
  });
});
