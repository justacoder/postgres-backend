const { QueryError } = require('../exceptions');

class Query {
  static start() {
    this.current = '';
    this.status = true;
    return this;
  }

  static end() {
    this.append(';');
    this.status = false;
    return this;
  }

  static reset() {
    this.status = false;
    this.current = '';
    return this;
  }

  static select( ...columns ) {
    this.append('SELECT');
    if (Array.isArray(columns) && columns.length) {
      this.append(columns.join(', '));
    } else {
      this.append('*')
    }
    return this;
  }

  static from( table ) {
    this.append(table);
    return this;
  }

  static where( ...conditions ) {
    return this;
  }

  static append(partialQueryString) {
    if (this.status) {
      this.current = `${this.current} ${partialQueryString}`.trim();
    } else {
      throw new QueryError('Query building hasn\'nt been started yet. Call start() when starting a new query.');
    }
    return this;
  }

  static create(type) {
    this
      .append('CREATE')
      .append(type)
    return this;
  }

  static withName(name) {
    this.append(name);
    return this;
  }

  static withColumns( ...columns ) {
    if (Array.isArray(columns) && columns.length) {
      this.append('(\n');
      columns.map(this.createColumn.bind(this)).join(',\n');
      this.append(')');
    }

    return this;
  }

  static createColumn(column) {
    const { name ,type, extra } = column;

    return (`${name} ${type} ${extra || ''} NOT NULL`);
  }
}

module.exports = Query;
