const { DB, HTTP } = require('../constants.js');
const FileProcessor = require('../file-processor.js');
const Database = require('../db');
const { HOST, PORT, PATH } = HTTP;

Database
  .configure()
  .connect()
  .seed()
  .catch((e) => {
    console.log(e)
  });

class Controller {

  static getSchema(req, res, next) {
    const { schema } = req.params;
    const found = Database.getSchema(schema);

    if (found) {
      res.status(200).json(found)
    } else {
      res.status(404).json({ message: `Schema ${schema} not found` });
    }
  }

  static info(req, res, next) {
    const resource = this.getObjectType(req);
    const table = Database.tables[resource];
    const info = Object
      .keys(table)
      .map((columnName) => ({ [columnName] : this.getColumnInfo(columnName, table[columnName])}))
      .reduce((prev, next) => Object.assign(prev, next), {});

    res.status(200).json(info);
  }

  static create( req, res, next ) {
    const { body, params, files } = req;
    const resource = this.getObjectType(req);

    Database
      .query(...Database.generateInsertQueryForTable(resource, req.body))
      .then((result) => {
        console.log(result);
        res.status(200).json({ message: 'success' });
      })
      .catch((e) => {
        console.log(e);
        res.status(500).json({ message: err.message });
      });
  }

  static readAll( req, res, next ) {
    const resource = this.getObjectType(req);

    Database
      .query(...Database.generateSelectQueryForTable(resource))
      .then(({ rows }) => {
        res.status(200).json(rows);
      })
      .catch((err) => {
        res.status(500).json({ error: err.message });
      });
  }

  static read( req, res, next ) {
    const resource = this.getObjectType(req);
    const query = Database.generateSelectQueryForTable(resource)

    const { params } = req;
    const { _id } = params;

    Database
      .query(...Database.generateSelectQueryForTable(resource, null, { lhs: '_id', rhs: _id, op: '=' }))
      .then(({ rows }) => {
        res.status(200).json(rows);
      })
      .catch((err) => {
        res.status(500).json({ error: err.message });
      });
  }

  static replace( req, res, next ) {
    const resource = this.getObjectType(req);
    const { body, params } = req;
    const { _id } = params;
    console.log('REQUEST PARAMS', params);
    console.log('REQUEST body', body);

    Database
      .query(...Database.generateUpdateQueryForTable(resource, body, { lhs: '_id', rhs: _id, op: '=' }))
      .then(({ rows }) => {
        res.status(200).json(rows);
      })
      .catch((err) => {
        res.status(500).json({ error: err.message });
      });
  }

  static modify( req, res, next ) {
    const resource = this.getObjectType(req);
    const { body, params } = req;
    const { _id } = params;
    console.log('REQUEST PARAMS', params);
    console.log('REQUEST body', body);

    Database
      .query(...Database.generateUpdateQueryForTable(resource, body, { lhs: '_id', rhs: _id, op: '=' }))
      .then(({ rows }) => {
        res.status(200).json(rows);
      })
      .catch((err) => {
        res.status(500).json({ error: err.message });
      });

  }

  static delete( req, res, next ) {
    const resource = this.getObjectType(req);
    const { _id } = req.params;

    Database
      .query(...Database.generateDeleteQueryForTable(resource, { _id }, [{ column: '_id', op: '=' }]))
      .then(({ rows }) => {
        res.status(200).json(rows);
      })
      .catch((err) => {
        res.status(500).json({ error: err.message });
      });

  }

  static getColumnInfo(columnName, columnType) {
    const { UPLOAD_COLUMNS, DATE_TIME_COLUMNS } = DB;

    if(columnName.includes('_id')) {
      return 'id';
    } else if(DATE_TIME_COLUMNS.includes(columnName)) {
      return 'datetime';
    } else if(UPLOAD_COLUMNS.includes(columnName)) {
      return 'file';
    } else if(columnType.includes('character')) {
      return 'text';
    } else if(columnType.includes('int')) {
      return 'number';
    }
  }

  static getObjectType(req) {
    console.log('REQUEST PATH =>>>>', req.path);
    return req.path.match(/[A-Za-z0-9]+/)[0];
  }
}

module.exports = Controller;
