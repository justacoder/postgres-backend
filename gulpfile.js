const gulp = require('gulp'),
config = require('./package.json'),
jest = require('gulp-jest').default,
spawn = require('child_process').spawn;
let node;

gulp.task('server', function() {
  if (node) {
    node.kill();
  }

  node = spawn('node', ['--inspect-brk', 'index.js']);
  node.on('close', function (code) {
    if (code === 8) {
      gulp.log('Error detected, waiting for changes...');
    }
  });
});

gulp.task('test', function() {
gulp
.src('src')
.pipe(jest(config.jest))
});

gulp.task('default', ['test', 'server'], function() {
  gulp.watch(['./index.js', './src/**/*.js' ], ['test', 'server']);
});

process.on('exit', function() {
  if (node) {
    node.kill();
  }
});
