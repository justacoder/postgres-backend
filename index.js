const app = require('./src/app.js');

app.start((port) => {
  console.log(`Server is up and running on port ${port}..`)
});
